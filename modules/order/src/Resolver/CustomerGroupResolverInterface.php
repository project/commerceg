<?php

namespace Drupal\commerceg_order\Resolver;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the interface for customer group resolvers.
 *
 * An order can belong to multiple groups for different reasons. However, there
 * should only be one group that is the customer group i.e. the group placing
 * the order as a customer; for example, an Organization.
 *
 * Customer group resolvers can determine which group is the customer group for
 * a given order.
 */
interface CustomerGroupResolverInterface {

  /**
   * Returns the customer group for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\group\Entity\GroupInterface|null
   *   The customer group, if resolved. Otherwise NULL, indicating that the
   *   next resolver in the chain should be called.
   */
  public function resolve(OrderInterface $order);

}
