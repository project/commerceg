<?php

namespace Drupal\commerceg_order\Resolver;

/**
 * Runs the resolvers in a chain until an order's customer group is resolved.
 *
 * We use a chain resolver because there could be multiple group types that
 * represent customer groups on behalf of which order can be placed. Each group
 * type provider can have a resolver that will detect whether the order is
 * placed on behalf of a group of the type that it's responsible, and if not
 * found it will proceed to the next resolver.
 *
 * Each resolver in the chain can be another chain, which is why this interface
 * extends the customer group resolver one.
 *
 * @see \Drupal\commerceg_order\Resolver\CustomerGroupResolverInterface
 */
interface ChainCustomerGroupResolverInterface extends
  CustomerGroupResolverInterface {

  /**
   * Adds a resolver to the chain.
   *
   * @param \Drupal\commerceg_order\Resolver\CustomerGroupResolverInterface $resolver
   *   The resolver.
   */
  public function addResolver(CustomerGroupResolverInterface $resolver);

  /**
   * Returns all resolvers in the chain.
   *
   * @return \Drupal\commerceg_order\Resolver\CustomerGroupResolverInterface[]
   *   The resolvers.
   */
  public function getResolvers();

}
