<?php

namespace Drupal\commerceg_order\Resolver;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Default implementation of the chain customer group resolver.
 */
class ChainCustomerGroupResolver implements
  ChainCustomerGroupResolverInterface {

  /**
   * The resolvers.
   *
   * @var \Drupal\commerceg_order\Resolver\CustomerGroupResolverInterface[]
   */
  protected $resolvers = [];

  /**
   * Constructs a new ChainCustomerGroupResolver object.
   *
   * @param \Drupal\commerceg_order\Resolver\CustomerGroupResolverInterface[] $resolvers
   *   The resolvers.
   */
  public function __construct(array $resolvers = []) {
    $this->resolvers = $resolvers;
  }

  /**
   * {@inheritdoc}
   */
  public function addResolver(CustomerGroupResolverInterface $resolver) {
    $this->resolvers[] = $resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getResolvers() {
    return $this->resolvers;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(OrderInterface $order) {
    foreach ($this->resolvers as $resolver) {
      $result = $resolver->resolve($order);
      if ($result) {
        return $result;
      }
    }
  }

}
