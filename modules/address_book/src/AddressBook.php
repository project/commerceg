<?php

namespace Drupal\commerceg_address_book;

use Drupal\commerceg\MachineName\Plugin\GroupContentEnabler;
use Drupal\group\Entity\GroupInterface;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\user\UserInterface;

use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation for a group's address book.
 */
class AddressBook implements AddressBookInterface {

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * The group content storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentStorageInterface
   */
  protected $contentStorage;

  /**
   * The profile storage.
   *
   * @var \Drupal\profile\ProfileStorageInterface
   */
  protected $profileStorage;

  /**
   * The profile type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $profileTypeStorage;

  /**
   * Constructs a new AddressBook object.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeBundleInfo $entity_type_bundle_info,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->contentStorage = $entity_type_manager->getStorage('group_content');
    $this->profileStorage = $entity_type_manager->getStorage('profile');
    $this->profileTypeStorage = $entity_type_manager->getStorage('profile_type');
  }

  /**
   * {@inheritdoc}
   *
   * @I Use content type settings for allowing multiple profiles per group
   *    type     : bug
   *    priority : normal
   *    labels   : address-book, config
   */
  public function allowsMultiple($profile_type_id) {
    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo('profile');

    return !empty($bundle_info[$profile_type_id]['multiple']);
  }

  /**
   * {@inheritdoc}
   *
   * @I Filter out profiles types that are not installed on groups
   *    type     : bug
   *    priority : low
   *    labels   : address-book
   *    notes    : Either check for installed plugins, or dispatch an event that
   *               allow the provider of the profiles e.g. B2B to remove
   *               non-related profile types. The latter is that profile types
   *               could be installed on irrelevant group types.
   */
  public function loadTypes() {
    return $this->profileTypeStorage->loadByProperties([
      'third_party_settings.commerce_order.customer_profile_type' => TRUE,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function loadAll(
    GroupInterface $group,
    $profile_type_id,
    array $available_countries = []
  ) {
    $contents = $this->contentStorage->loadByGroup(
      $group,
      GroupContentEnabler::PROFILE . ':' . $profile_type_id
    );
    if (!$contents) {
      return [];
    }

    $profiles = [];
    foreach ($contents as $content) {
      $profile = $content->getEntity();
      // Filter out profiles with unavailable countries.
      if (!$this->isAvailable($profile, $available_countries)) {
        continue;
      }
      $profiles[$profile->id()] = $profile;
    }

    return $profiles;
  }

  /**
   * {@inheritdoc}
   */
  public function load(
    GroupInterface $group,
    $profile_type_id,
    array $available_countries = []
  ) {
    // @I Optimize query so that we don't have to load all profiles
    //    type     : improvement
    //    priority : normal
    //    labels   : address-book, performance
    $profiles = $this->loadAll($group, $profile_type_id, $available_countries);
    if (!$profiles) {
      return;
    }

    foreach ($profiles as $profile) {
      if ($profile->isDefault()) {
        return $profile;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function needsCopy(ProfileInterface $profile) {
    return (bool) $profile->getData('copy_to_address_book', FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function copy(
    ProfileInterface $profile,
    GroupInterface $group,
    UserInterface $user
  ) {
    $address_book_profile = $this->getAddressBookProfileForUpdate($profile);

    // If we have an existing profile to update in the address book, do that.
    if ($address_book_profile) {
      $address_book_profile->populateFromProfile($profile);
      $this->profileStorage->save($address_book_profile);
    }
    // Otherwise, create a new one.
    else {
      $address_book_profile = $this->createProfileInAddressBook($profile);
    }

    $profile->unsetData('copy_to_address_book');
    $profile->setData('address_book_profile_id', $address_book_profile->id());
    $this->profileStorage->save($profile);
  }

  /**
   * Checks if the given profile is available.
   *
   * If the list of available countries is restricted, the profile address
   * is checked against it.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile.
   * @param array $available_countries
   *   List of country codes. If empty, all countries will be available.
   *
   * @return bool
   *   TRUE if the profile is available, FALSE otherwise.
   */
  protected function isAvailable(
    ProfileInterface $profile,
    array $available_countries
  ) {
    if (!$available_countries) {
      return TRUE;
    }

    $address = $profile->get('address')->first();
    $country_code = $address ? $address->getCountryCode() : 'ZZ';

    return in_array($country_code, $available_countries);
  }

  /**
   * Returns the address book profile to update for the given profile.
   *
   * When we copy a profile to the address book, we first check if we already
   * have an existing profile in the address book associated with the given
   * profile. In that case we update the existing profile instead of creating a
   * new one.
   *
   * We also update the existing profile if it is not allowed to have multiple
   * profiles of this type in a group.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile to get the existing address book profile for.
   *
   * @return \Drupal\profile\Entity\ProfileInterface|null
   *   The existing address book profile to update, or NULL if none was found.
   */
  protected function getAddressBookProfileForUpdate(ProfileInterface $profile) {
    $existing_profile = NULL;

    // Check whether we have a profile already copied to the address book once.
    $existing_profile_id = $profile->getData('address_book_profile_id');
    if ($existing_profile_id) {
      $existing_profile = $this->profileStorage->load($existing_profile_id);
    }

    // If not and we can't have multiple profiles of this type in a group, get
    // the existing profile in the address book - if any.
    if (!$existing_profile && !$this->allowsMultiple($profile->bundle())) {
      $existing_profile = $this->load($group, $profile->bundle());
    }

    return $existing_profile;
  }

  /**
   * Creates a new profile in the address book as a copy of the given profile.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile.
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group.
   * @param \Drupal\user\UserInterface $user
   *   The user that will be the owner of the profile.
   *
   * @return \Drupal\profile\Entity\ProfileInterface
   *   The created address book profile.
   */
  protected function createProfileInAddressBook(
    ProfileInterface $profile,
    GroupInterface $group,
    UserInterface $user
  ) {
    // Create the profile.
    $address_book_profile = $profile->createDuplicate();
    $address_book_profile->setOwnerId($user->id());
    $address_book_profile->unsetData('copy_to_address_book');
    $this->profileStorage->save($address_book_profile);

    // Add it as content to the group.
    $content = $this->contentStorage->createForEntityInGroup(
      $address_book_profile,
      $group,
      GroupContentEnabler::PROFILE . ':' . $profile->bundle()
    );
    $this->contentStorage->save($content);

    return $address_book_profile;
  }

}
