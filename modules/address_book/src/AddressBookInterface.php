<?php

namespace Drupal\commerceg_address_book;

use Drupal\group\Entity\GroupInterface;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\user\UserInterface;

/**
 * Represents a group's address book.
 *
 * An address book is a collection of profile entities belonging to a
 * group. Profiles can be of different types, and groups have a
 * default profile for each type.
 *
 * Anonymous profiles (uid: 0) belong to a parent entity (e.g. an order or
 * payment method) and maintain a record of group information for
 * use in that entity's context. They are not part of an address book but
 * can be copied to be saved to a group's address book.
 *
 * We separate profiles in this manner to ensure data integrity for the
 * orders, payment methods, etc., preventing the group information
 * saved to them from being edited or deleted in other contexts.
 *
 * The intention is to maintain the same behavior with the Address Book
 * functionality provided by Commerce Order, and a similar interface as much as
 * possible.
 *
 * @see \Drupal\commerce_order\AdressBookInterface
 *
 * @I Review UI function e.g. `hasUi` method
 *    type     : improvement
 *    priority : normal
 *    labels   : ui
 */
interface AddressBookInterface {

  /**
   * Returns whether a group can have multiple profiles of this type.
   *
   * @param string $profile_type_id
   *   The profile type ID.
   *
   * @return bool
   *   TRUE if a group can have multiple profiles of this type, FALSE
   *   otherwise.
   */
  public function allowsMultiple($profile_type_id);

  /**
   * Loads the profile types used by the address book.
   *
   * Only customer profile types are included.
   *
   * @return \Drupal\profile\Entity\ProfileTypeInterface[]
   *   The profile types, keyed by profile type ID.
   */
  public function loadTypes();

  /**
   * Loads all profiles for the given group.
   *
   * Ensures that the loaded profiles are available, by filtering
   * them against $available_countries.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group.
   * @param string $profile_type_id
   *   The profile type ID.
   * @param array $available_countries
   *   List of country codes. If empty, all countries will be available.
   *
   * @return \Drupal\profile\Entity\ProfileInterface[]
   *   The available profiles, keyed by profile ID.
   */
  public function loadAll(
    GroupInterface $group,
    $profile_type_id,
    array $available_countries = []
  );

  /**
   * Returns the default profile of the given type for the group.
   *
   * Falls back to the newest published profile if a default profile is not
   * found.
   *
   * Primarily used for profile types which only allow a single profile per
   * group.
   *
   * Ensures that the loaded profile is available, by filtering it against
   * $available_countries. If the loaded profile is not available, NULL will be
   * returned instead.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group.
   * @param string $profile_type_id
   *   The profile type ID.
   * @param array $available_countries
   *   List of country codes. If empty, all countries will be available.
   *
   * @return \Drupal\profile\Entity\ProfileInterface|null
   *   The profile, or NULL if none found.
   */
  public function load(
    GroupInterface $group,
    $profile_type_id,
    array $available_countries = []
  );

  /**
   * Determines whether the profile needs to be copied to the address book.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile.
   *
   * @return bool
   *   TRUE if the profile needs to be copied to the group's address book,
   *   FALSE otherwise.
   */
  public function needsCopy(ProfileInterface $profile);

  /**
   * Copies the profile to the group's address book.
   *
   * If the group is allowed to have multiple profiles of this type, the given
   * profile will be duplicated and assigned to it. If the given profile was
   * already copied to the group's address book once, the matching address book
   * profile will be updated instead.
   *
   * If the group is only allowed to have a single profile of this type,
   * the default profile will be loaded (created if missing) and updated.
   *
   * When a profile is created (instead of updating an existing one), the given
   * user is added as the owner of the profile.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile.
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group.
   * @param \Drupal\user\UserInterface $user
   *   The user that will be the owner of the profile.
   */
  public function copy(
    ProfileInterface $profile,
    GroupInterface $group,
    UserInterface $user
  );

}
