<?php

namespace Drupal\commerceg\Exception;

/**
 * Exception when a configuration item is not found as expected during runtime.
 */
class RuntimeConfigurationException extends \RuntimeException {
}
