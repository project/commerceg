<?php

namespace Drupal\commerceg\Group;

/**
 * Facilitates loading of group content type entities.
 *
 * We don't want to replace the Group Content Type storage and add methods there
 * as we may make it more difficult for applications that need to do the same.
 * We could request to add these methods to the Group Content Type storage in
 * the Group module.
 *
 * Group content enabler plugin providers of the Commerce Group ecosystem can
 * extend this service to provide plugin-specific methods.
 */
interface ContentTypeLoaderInterface {

  /**
   * Returns the content type for the given plugin ID and group type ID.
   *
   * There will always only exist one content type for the given plugin on a
   * single group type.
   *
   * @param string $plugin_id
   *   The ID of the group content enabler plugin.
   * @param string $group_type_id
   *   The ID of the group type (bundle).
   *
   * @return \Drupal\group\Entity\GroupContentTypeInterface|null
   *   The group content type, or NULL if the plugin with the given ID is not
   *   installed on the group type with the given ID.
   *
   * @throws \Drupal\commerceg\Exception\RuntimeConfigurationException
   *   When the given group content enabler plugin is not installed on the given
   *   group type, the plugin does not exist, or the group type does not exist.
   */
  public function loadByPluginIdAndGroupTypeId(
    string $plugin_id,
    string $group_type_id
  );

}
