<?php

namespace Drupal\commerceg\Group;

use Drupal\commerceg\Exception\RuntimeConfigurationException;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation for the content type loader service.
 */
class ContentTypeLoader implements ContentTypeLoaderInterface {

  /**
   * The group content type storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface
   */
  protected $contentTypeStorage;

  /**
   * Constructs a new ContentTypeLoader object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->contentTypeStorage = $entity_type_manager
      ->getStorage('group_content_type');
  }

  /**
   * {@inheritdoc}
   */
  public function loadByPluginIdAndGroupTypeId(
    string $plugin_id,
    string $group_type_id
  ) {
    $types = $this->contentTypeStorage->loadByContentPluginId($plugin_id);

    if (!$types) {
      throw new RuntimeConfigurationException(sprintf(
        'The "%s" plugin is not installed on any group type, or the plugin does not exist.',
        $plugin_id
      ));
    }

    foreach ($types as $type) {
      if ($type->getGroupType()->id() !== $group_type_id) {
        continue;
      }

      return $type;
    }

    throw new RuntimeConfigurationException(sprintf(
      'The "%s" plugin is not installed on the "%s" group type, or the group type does not exist.',
      $plugin_id,
      $group_type_id
    ));
  }

}
