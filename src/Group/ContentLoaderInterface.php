<?php

namespace Drupal\commerceg\Group;

use Drupal\Core\Entity\EntityInterface;

/**
 * Facilitates loading of group content entities.
 *
 * We don't want to replace the Group Content storage and add methods there as
 * we may make it more difficult for applications that need to do the same. We
 * could request to add these methods to the Group Content storage in the Group
 * module.
 *
 * Group content enabler plugin providers of the Commerce Group ecosystem can
 * extend this service to provide plugin-specific methods.
 */
interface ContentLoaderInterface {

  /**
   * Returns content entities for the given entity and plugin/group type IDs.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to load the group content for.
   * @param string $plugin_id
   *   The ID of the group content enabler plugin.
   * @param string $group_type_id
   *   The ID of the group type (bundle).
   *
   * @return \Drupal\group\Entity\GroupContentInterface[]
   *   The group content entities for the entity.
   *
   * @throws \Drupal\commerceg\Exception\RuntimeConfigurationException
   *   When the given group content enabler plugin is not installed on the given
   *   group type, the plugin does not exist, or the group type does not exist.
   */
  public function loadForEntityByPluginIdAndGroupTypeId(
    EntityInterface $entity,
    string $plugin_id,
    string $group_type_id
  );

}
