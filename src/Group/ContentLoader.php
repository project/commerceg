<?php

namespace Drupal\commerceg\Group;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation for the content loader service.
 */
class ContentLoader implements ContentLoaderInterface {

  /**
   * The Commerce Group content type loader.
   *
   * @var \Drupal\commerceg\Group\ContentTypeLoaderInterface
   */
  protected $contentTypeLoader;

  /**
   * The group content storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentStorageInterface
   */
  protected $contentStorage;

  /**
   * Constructs a new ContentLoader object.
   *
   * @param \Drupal\commerceg\Group\ContentTypeLoaderInterface $content_type_loader
   *   The Commerce Group content type loader.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ContentTypeLoaderInterface $content_type_loader,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    // Keep in mind that we should use Lazy Services if we add more methods that
    // do not use the Content Type Loader.
    // https://symfony.com/doc/current/service_container/lazy_services.html
    $this->contentTypeLoader = $content_type_loader;

    $this->contentStorage = $entity_type_manager->getStorage('group_content');
  }

  /**
   * {@inheritdoc}
   */
  public function loadForEntityByPluginIdAndGroupTypeId(
    EntityInterface $entity,
    string $plugin_id,
    string $group_type_id
  ) {
    $content_type = $this->contentTypeLoader->loadByPluginIdAndGroupTypeId(
      $plugin_id,
      $group_type_id
    );

    return $this->contentStorage->loadByProperties([
      'type' => $content_type->id(),
      'entity_id' => $entity->id(),
    ]);
  }

}
